# Bachelor Thesis Webservice by Jacob Fehn

## Usage
### Requirements
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the required packages.

```bash
pip install -r requirements.txt
```